#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
    int mypos = 0;
    int enemypos = 80;
    int damage = 0; // start with zero damage
    bool alive = true;

    // enemy throws a grende at me
    if (enemypos <= 80 && enemypos > 60){
        damage = 50;
    } else if (enemypos <= 60 && enemypos > 40){
        damage = 75;
    } else if (enemypos <= 40 && enemypos > 10){
        damage = 85;
    } else if (enemypos <= 10 ){
        damage = 100; // dead
    }

    cout << "Damage: " << damage << "\n";

    // example of substr
    string sen = "The rain in Spain falls mainly in the plain";

    string word = sen.substr(4,4);
    cout << "Word found: " << word << "\n";

    // example of find

    int s = sen.find("Spain");
    cout << "Word found at " << s << "\n";

    // for loop
    int totalBP = 0;
    int i, nreadings;
    float average;
    cout << "How many readings per patient? ";
    cin >> nreadings;
    int BP, badBP = 0;
    for(i=0; i< nreadings; i++){
        cout << "Enter BP:" ;
        cin >> BP;
        if (BP < 30 || BP > 70) badBP = badBP + 1;
        totalBP = totalBP + BP;
    }
    average = totalBP / nreadings;
    cout << "Average reading: " << average << endl;
    cout << "Bad BP: " << badBP << endl;

    // WHILE LOOP
    totalBP = 0;
    int j = 1;
    int nmax;
    cout << "How many readings per patient? ";
    cin >> nmax;
    while (j <= nmax) {
        cout << "Enter BP:" ;
        cin >> BP;
        totalBP = totalBP + BP;
        j++;
    }
    average = totalBP / nmax;
    cout << "Average reading: " << average << endl;

} 